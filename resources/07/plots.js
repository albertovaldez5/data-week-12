function handleSubmit() {
    var city = d3.select("#cityInput").node().value;
    console.log(city);
    d3.select("#cityInput").node().value = "";
    buildPlot(city);
}
function getTimes(owmData) {
    return owmData.list.map(function (x) { return x.dt_txt; });
}
function getTemps(owmData) {
    return owmData.list.map(function (x) { return x.main.temp; });
}
function buildPlot(city) {
    var apiKey = API_KEY;
    var url = "https://api.openweathermap.org/data/2.5/forecast?q=".concat(city, "&appid=").concat(apiKey, "&units=imperial");
    // Replacing API based request to local public files
    d3.json(url).then(function (data) {
        times = getTimes(data);
        temps = getTemps(data);
        var trace1 = {
            type: "scatter",
            mode: "lines",
            x: times,
            y: temps,
            line: {
                color: "#17BECF"
            }
        };
        var plotData = [trace1];
        var layout = {
            title: "".concat(data.city.name, " 5-day forecast")
        };
        Plotly.newPlot("weather", plotData, layout);
    });
}
buildPlot("Mexico");
var button = d3.select("#submit");
button.on("click", handleSubmit);
