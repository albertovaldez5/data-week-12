var filteredMovies = topMovies.filter(function (movie) { return movie.imdbRating > 8.9; });
var titles = filteredMovies.map(function (movie) { return movie.title; });
var metascores = filteredMovies.map(function (movie) { return movie.metascore; });
var trace = {
    x: titles,
    y: metascores,
    type: "bar"
};
var data = [trace];
var layout = {
    title: "Top Movies",
    xaxis: {
        title: "Title"
    },
    yaxis: {
        title: "Score"
    }
};
Plotly.plot("bar-plot", data, layout);
