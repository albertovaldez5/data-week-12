import json
import requests as rq

with open("resources/keys/keys.json", 'rb') as file:
    key = json.load(file)[0]["key"]

path = "./resources/07"
cities = ["Mexico", "Madrid", "London", "Seattle", "Seoul"]
for city in cities:
    url = f"https://api.openweathermap.org/data/2.5/forecast?q={city}&appid={key}&units=imperial"
    r = rq.get(url).json()
    with open(f"{path}/{city}.json", "w") as file:
        json.dump(r, file)
    print(r)
