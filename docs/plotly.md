- [Plotly](#plotly)
  - [Importing Plotly](#importing-plotly)
  - [Using Plotly](#using-plotly)
- [Filtering Data](#filtering-data)
  - [Filter and Map](#filter-and-map)
  - [Example: Filter](#example-filter)
- [Sorting Data](#sorting-data)
  - [Sorting](#sorting)
  - [Slicing](#slicing)
  - [Example: Slicing](#example-slicing)
- [CORS](#cors)
- [Multiple Traces](#multiple-traces)
- [Dropdown Menus](#dropdown-menus)
- [Promises](#promises)
  - [JSON Requests](#json-requests)



<a id="plotly"></a>

# Plotly

Plotly is a Javascript library that will help us draw plots and charts using SVG graphics in the web browser. The best way to use it is to use the documentation and learn the library instead of solving problems as they arise.

Having visualizations that allows us to generate results over and over again can be very useful for any industry as the user can interact with it as a web application, not just data <sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup>.

Plotly has interfaces for many programming languages but we will be using the Javascript one <sup><a id="fnr.2" class="footref" href="#fn.2" role="doc-backlink">2</a></sup>.


<a id="importing-plotly"></a>

## Importing Plotly

We can import Plotly in our HTML header.

```html
<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
```

Then our complete HTML template imports the scripts in the body.

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>First Chart</title>
  <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
</head>
<body>
  <div id="bar-plot"></div>
  <script src="../resources/plots.js"></script>
</body>
</html>
```


<a id="using-plotly"></a>

## Using Plotly

We need to specify an environment for our plot using:

1.  ID for an existing `div` element.
2.  Layout: Object that contains certain properties.
3.  Data: An array of traces, which are objects that contain data for a trace.

A `trace` will have a type and arrays of data we will plot.

```typescript
var eyeColor = ["Brown", "Brown", "Brown", "Brown", "Brown",
  "Brown", "Brown", "Brown", "Green", "Green",
  "Green", "Green", "Green", "Blue", "Blue",
  "Blue", "Blue", "Blue", "Blue"];

var eyeFlicker = [26.8, 27.9, 23.7, 25, 26.3, 24.8,
  25.7, 24.5, 26.4, 24.2, 28, 26.9,
  29.1, 25.7, 27.2, 29.9, 28.5, 29.4, 28.3];

var trace = {
    type: "bar",
    x: eyeColor,
    y: eyeFlicker
}
```

We use an array of traces to create the plot. We can have as many trace types as we want in an array.

```typescript

var data = [trace];
var layout = {
    title: "Eye flicker",
    xaxis: {
      title: {
        text: 'Eye'
      }
    },
    yaxis: {
      title: {
        text: 'Flicker'
      }
    }
}
Plotly.newPlot("bar-plot", data, layout);
```

Plotly will label our data by defaul as giving us a few interactivity options inside the plot and some other at the top right of the plot.

**NOTE**: The results are SVG files but we want to include them in this document as PNG files. We are going to use Python to get a quick screenshot of our result. <sup><a id="fnr.3" class="footref" href="#fn.3" role="doc-backlink">3</a></sup>

<div class="org" id="orgd99ac76">

<div id="orge9fbea6" class="figure">
<p><img src="../resources/01.png" alt="01.png" width="700" />
</p>
</div>

</div>


<a id="filtering-data"></a>

# Filtering Data

We can process our data with Javascript before using Plotly. This means combining builtin functions and our own regular or arrow functions to filter and map the data.


<a id="filter-and-map"></a>

## Filter and Map

We will create an arrow function to use the `filter` function to get only the data we want. Then `map` gives us a new array based on the function we give it.

In this case an arrow function with only one expression can be used as the callback for the filter, where `movie` is our argument and the expression will serve as a `true/false` index for our array.

Using `map` this way will extract all values of given property of our `movie` object.

```typescript
var filteredMovies = topMovies.filter(movie => movie.imdbRating > 8.9);
var titles = filteredMovies.map(movie => movie.title);
var metascores = filteredMovies.map(movie => movie.metascore);
```

Now we can plot our data using the filtered arrays.

```typescript
var filteredMovies = topMovies.filter(movie => movie.imdbRating > 8.9);
var titles = filteredMovies.map(movie => movie.title);
var metascores = filteredMovies.map(movie => movie.metascore);
var trace = {
    x:titles,
    y: metascores,
    type: "bar"
};
var data = [trace];
var layout = {
    title: "Top Movies",
    xaxis: {
        title: "Title"
    },
    yaxis: {
        title: "Score"
    }
};
Plotly.plot("bar-plot", data, layout);
```


<a id="example-filter"></a>

## Example: Filter

This time we want to filter data that looks like this (incomplete example).

```typescript
var top15Cities = [
  {
    "Rank": 1,
    "City": "San Antonio ",
    "State": "Texas",
    "Increase_from_2016": "24208",
    "population": "1511946"
  },
  {
    "Rank": 2,
    "City": "Phoenix ",
    "State": "Arizona",
    "Increase_from_2016": "24036",
    "population": "1626078"
  },
```

We want to filter the top 15 cities with a population increase greater than 15,000. We will use `parseInt` to make sure the data are integers.

```typescript
var biggestCities = top15Cities.filter(city => parseInt(city.population) > 15000);
var names = biggestCities.map(city => city.City);
var populations = biggestCities.map(city => parseInt(city.population));
var trace = {
    type: "bar",
    x: names,
    y: populations
};
var data = [trace];
var layout = {
    title: "Most Populated",
    xaxis: {
        title: "City"
    },
    yaxis: {
        title: "Population"
    }
};
Plotly.plot("bar-plot", data, layout);
```

<div class="org" id="org53a1f56">

<div id="orgcea1f1e" class="figure">
<p><img src="../resources/04.png" alt="04.png" width="700" />
</p>
</div>

</div>


<a id="sorting-data"></a>

# Sorting Data


<a id="sorting"></a>

## Sorting

Sorting means changing the order on which the elements of an array are stored based on some parameter.

Sorting numbers is pretty simple, as the default callback of the `sort` function is to compare numbers.

```typescript
var numArray = [2, 1, 3];
numArray.sort();
console.log(numArray);
```

    [ 1, 2, 3 ]

Sorting functions will compare two values. If the return value is zero, it will do nothing, if the value is positive it will reverse the order of the arguments, if it&rsquo;s negative it will do nothing.

The following function is an example of the default callback function for `sort`.

```typescript
function compareFunctionAsc(firstEl, secondEl){
    return firstEl - secondEl;
}
```

Positive reverts the order, anything else preserves it. So an inverse callback function would look like this:

```typescript
var numArray = [2, 1, 3];
numArray.sort((a, b) => b - a);
console.log(numArray);
```

    [ 3, 2, 1 ]

**NOTE**: Sort will change the array in-place, it doesn&rsquo;t return a new one.


<a id="slicing"></a>

## Slicing

Gets a new array from the indices given to the `slice` function. Arrays are not circluar in Javascript, so we should avoid using negative indices for slicing.

```typescript
var names = ["Jane", "John", "Jimbo", "Jaed"];
var left = names.slice(0, 2);
var right = names.slice(2, 4);
console.log(left);
console.log(right);
```

    [ 'Jane', 'John' ]
    [ 'Jimbo', 'Jaed' ]

Another example of sorting and slicing.

```typescript
numArray = [9.9, 6.1, 17.1, 22.7, 4.6, 8.7, 7.2];
var descending = numArray.sort((a, b) => b - a);
console.log(descending);
var ascending = descending.sort((a, b) => a - b);
var sliced = ascending.slice(0, 5);
console.log(sliced);
```


<a id="example-slicing"></a>

## Example: Slicing

We are going to build a visualization from our data scripts.

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Greek gods</title>
    <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
</head>
<body>
    <div id="plot"></div>
    <script src="data.js"></script>
    <script src="plots.js"></script>
    <script src="sliceSort.js"></script>
</body>
</html>
```

We are going to filter data that looks like this.

```typescript
var data = [{
  greekName: "Amphitrite",
  greekSearchResults: 387000,
  pair: "Amphitrite-Salacia",
  romanName: "Salacia",
  romanSearchResults: 47500
},
{
  greekName: "Ananke",
  greekSearchResults: 73600,
  pair: "Ananke-Necessitas",
  romanName: "Necessitas",
  romanSearchResults: 386000
},
```

We will sort the data by Greek search results. Then get the top 10 by sclicing the first 10 objects.

Because of how Plotly is configured by default, we will reverse the array first so we can display the data from top to bottom.

Then we will use `map` to get the specific values of each object and give them to our `trace`. Then we set our layout to specific parameters and create the plot.

```typescript
var sorted = data.sort((a, b) => b.greekSearchResults - a.greekSearchResults);
var sliced = sorted.slice(0, 10);
var reversed = sliced.reverse();
var trace = {
    y: reversed.map(god => god.greekName),
    x: reversed.map(god => god.greekSearchResults),
    text: reversed.map(god => god.greekName),
    name: "Greek",
    type: "bar",
    orientation: "h"
}
var data = [trace];
var layout = {
    title:"Greek Data",
    margin: {
        l: 150,
        r: 150,
        t: 150,
        b: 150
    }
};
Plotly.newPlot("plot", data, layout);
```

<div class="org" id="org4085c77">

<div id="orgd849486" class="figure">
<p><img src="../resources/06.png" alt="06.png" width="700" />
</p>
</div>

</div>


<a id="cors"></a>

# CORS

Cross-Origin Resource Sharing <sup><a id="fnr.4" class="footref" href="#fn.4" role="doc-backlink">4</a></sup> tells a server which origins have permissions to load sources. This is made for security reasons to prevent from reading or writing on local files from the user machine.

Running a local server with `npm`<sup><a id="fnr.5" class="footref" href="#fn.5" role="doc-backlink">5</a></sup> or Python&rsquo;s `http.server`<sup><a id="fnr.6" class="footref" href="#fn.6" role="doc-backlink">6</a></sup> allows us to bypass these security restrictions **for local development only**, which means it should not be used for production code or opening it to the public.

We communicate with the local server via a `request-response` model, where the user sends a requests to the server and the server returns the data. This is why APIs are important.


<a id="multiple-traces"></a>

# Multiple Traces

Plotly allows us to show multiple traces in a same plot. The `data` array will hold all the traces we need it to plot. We can have traces with multiple lengths and Plotly will automatically use the longest one to plot the data.

For example, if we had relational data, which in this case are two different names that refer to the same character, we can create an array that references the names and two different arrays that reference two different values that relate to the character name.

```typescript
var names = data.map((god) => `${god.greekName}-${god.romanName}`);
var trace1 = {
    type: "bar",
    name: "Greek",
    x: names,
    y: data.map((god) => god.greekSearchResults),
    text: data.map((god) => god.greekName)
};
var trace2 = {
    type: "bar",
    name: "Roman",
    x: names,
    y: data.map((god) => god.romanSearchResults),
    text: data.map((god) => god.romanName)
};
var data = [trace1, trace2];
var layout = {
    title: "Greek v.s. Roman",
    barmode: "group"
};
Plotly.newPlot("plot", data);
```

<div class="org" id="org18b4a42">

<div id="org72e0b1c" class="figure">
<p><img src="../resources/02.png" alt="02.png" width="700" />
</p>
</div>

</div>


<a id="dropdown-menus"></a>

# Dropdown Menus

Whenever we use Plotly, we normally follow this footprint:

1.  A default plot is rendered on the page.
2.  A change takes place in the DOM when a dropdown menu item is selected.
3.  A function is triggered with the DOM&rsquo;s element value as its argument.
4.  The function uses Plotly `restyle` method to modify the existing plot.

We will start with a default behaviour from the `init` function. Then we add an `Event listener` to the code, which will call an `Event Handler` whenever it&rsquo;s triggered.

Our function will change plot by using Plotly&rsquo;s `restyle` in this case.

```typescript
var us = Object.values(dataLabel.us);
var uk = Object.values(dataLabel.uk);
var canada = Object.values(dataLabel.canada);
var musicLabels = Object.keys(dataLabel.us);
function init() {
    var trace = {
        values: us,
        labels: musicLabels,
        type: 'pie'
    };
    var layout = {
        title: "Music Labels"
    };
    var data = [trace];
    Plotly.newPlot("pie", data, layout);
}
function updatePage(event) {
    var dataset = d3.select("#selectOption").property("value");
    var values = us;
    if (dataset === "uk") {
        values = uk;
    }
    else if (dataset === "us") {
        values = us;
    }
    else if (dataset === "canada") {
        values = canada;
    }
    Plotly.restyle("pie", "values", [values]);
}
d3.select("#selectOption").on("change", updatePage);
init();
```

<div class="org" id="org43d2666">

<div id="orgbbbd75d" class="figure">
<p><img src="../resources/05.png" alt="05.png" width="700" />
</p>
</div>

</div>


<a id="promises"></a>

# Promises

Javascript promises <sup><a id="fnr.7" class="footref" href="#fn.7" role="doc-backlink">7</a></sup> are processes that will happen in the future. They can have two results: `fullfilled success` or `fullfilled rejected`.

For example, `d3.json(url)` is a promise while it&rsquo;s not completed. If we try to log a promise, we will get a `Promise` object which has `PromiseState` and `PromiseResult` fields.

The `then` method allows us to work with promises when they are fullfilled. It will execute a callback once the promise is fullfilled.

```typescript
d3.json(url).then((data) => console.log(data));
```


<a id="json-requests"></a>

## JSON Requests

We want to make a JSON request to an API whenever the user interacts with a input field.

Then we will transform the data from the JSON response and plot it with Plotly. We are going to make a new plot each time as it is easier than restyling every single part of it.

The `API_KEY` constant is loaded from a file named `config.js` which looks like this (replace gibberish with actual API key).

```typescript
API_KEY = "1234567890thisistheapikey1234567890"
```

The HTML body has these scripts now.

```html
<script src="config.js"></script>
<script src="plots.js"></script>
```

The `plots.js` script will look like this.

```typescript
function handleSubmit() {
    var city = d3.select("#cityInput").node().value;
    console.log(city);
    d3.select("#cityInput").node().value = "";
    buildPlot(city);
}
function getTimes(owmData) {
    return owmData.list.map(function (x) { return x.dt_txt; });
}
function getTemps(owmData) {
    return owmData.list.map(function (x) { return x.main.temp; });
}
function buildPlot(city) {
    var apiKey = API_KEY;
    var url = "https://api.openweathermap.org/data/2.5/forecast?q=".concat(city, "&appid=").concat(apiKey, "&units=imperial");
    // Replacing API based request to local public files
    d3.json(url).then(function (data) {
        times = getTimes(data);
        temps = getTemps(data);
        var trace1 = {
            type: "scatter",
            mode: "lines",
            x: times,
            y: temps,
            line: {
                color: "#17BECF"
            }
        };
        var plotData = [trace1];
        var layout = {
            title: "".concat(data.city.name, " 5-day forecast")
        };
        Plotly.newPlot("weather", plotData, layout);
    });
}
buildPlot("Mexico");
var button = d3.select("#submit");
button.on("click", handleSubmit);
```

<div class="org" id="org0fd9de9">

<div id="org1bd6f88" class="figure">
<p><img src="../resources/07.png" alt="07.png" width="700" />
</p>
</div>

</div>

## Footnotes

<sup><a id="fn.1" class="footnum" href="#fnr.1">1</a></sup> <https://www.forbes.com/sites/benkerschberg/2014/04/30/five-key-properties-of-interactive-data-visualization/?sh=4224c228589e>

<sup><a id="fn.2" class="footnum" href="#fnr.2">2</a></sup> <https://plotly.com/javascript/>

<sup><a id="fn.3" class="footnum" href="#fnr.3">3</a></sup> [Python Scraper](./scraper.md)

<sup><a id="fn.4" class="footnum" href="#fnr.4">4</a></sup> <https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS>

<sup><a id="fn.5" class="footnum" href="#fnr.5">5</a></sup> <https://www.npmjs.com/package/http-server>

<sup><a id="fn.6" class="footnum" href="#fnr.6">6</a></sup> <https://docs.python.org/3/library/http.server.html>

<sup><a id="fn.7" class="footnum" href="#fnr.7">7</a></sup> <https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise>
