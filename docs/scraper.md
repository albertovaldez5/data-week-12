- [Introduction](#introduction)
- [Taking a screenshot](#taking-a-screenshot)



<a id="introduction"></a>

# Introduction

In this document we are going to code a few tools to retrieve data from the results of other documents and call the scripts from the command line.


<a id="taking-a-screenshot"></a>

# Taking a screenshot

We are going to use Selenium to open our webpage and take a screenshot of it, all while in headless mode. Then we are going to save it to a path. This script will take command line arguments so we can use it with whatever inputs and outputs we want.

```python
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from argparse import ArgumentParser, Namespace
from selenium import webdriver
from pathlib import Path
from time import sleep


def main(argv: Namespace):
    if "http" in argv.html:
        html = argv.html
    else:
        html = f"file://{Path(argv.html).resolve()}"
    png = argv.png
    browser = setup_browser()
    browser.get(html)
    sleep(1)
    browser.save_screenshot(f"{png}")
    browser.quit()
    print(png.strip(), end="")


def setup_browser() -> webdriver.Chrome:
    """Start aget_browserome browser."""
    options = Options()
    options.headless = True
    service = Service(ChromeDriverManager().install())
    return webdriver.Chrome(service=service, options=options)


if __name__ == "__main__":
    args = ArgumentParser(
        prog="Screenshot Selenium",
        usage="python screen.py index.html index.png",
        description="Take a screenshot of given html and store it in png",
    )
    args.add_argument(
        "html", metavar="html", help="relative path of html, will be made absolute"
    )
    args.add_argument("png", metavar="png", help="relative path of the png result")
    main(args.parse_args())
```
