var names = data.map(function (god) { return "".concat(god.greekName, "-").concat(god.romanName); });
var trace1 = {
    type: "bar",
    name: "Greek",
    x: names,
    y: data.map(function (god) { return god.greekSearchResults; }),
    text: data.map(function (god) { return god.greekName; })
};
var trace2 = {
    type: "bar",
    name: "Roman",
    x: names,
    y: data.map(function (god) { return god.romanSearchResults; }),
    text: data.map(function (god) { return god.romanName; })
};
var data = [trace1, trace2];
var layout = {
    title: "Greek v.s. Roman",
    barmode: "group"
};
Plotly.newPlot("plot", data);
