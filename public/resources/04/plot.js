var biggestCities = top15Cities.filter(function (city) { return parseInt(city.population) > 15000; });
var names = biggestCities.map(function (city) { return city.City; });
var populations = biggestCities.map(function (city) { return parseInt(city.population); });
var trace = {
    type: "bar",
    x: names,
    y: populations
};
var data = [trace];
var layout = {
    title: "Most Populated",
    xaxis: {
        title: "City"
    },
    yaxis: {
        title: "Population"
    }
};
Plotly.plot("bar-plot", data, layout);
