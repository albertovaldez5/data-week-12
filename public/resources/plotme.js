$(document).ready(function () {
    var sorted = data.sort(function (a, b) { return b.greekSearchResults - a.greekSearchResults; });
    var sliced = sorted.slice(0, 10);
    var reversed = sliced.reverse();
    var trace = {
        y: reversed.map(function (god) { return god.greekName; }),
        x: reversed.map(function (god) { return god.greekSearchResults; }),
        text: reversed.map(function (god) { return god.greekName; }),
        name: "Greek",
        type: "bar",
        orientation: "h"
    };
    var data = [trace];
    var layout = {
        title: "Greek Data",
        margin: {
            l: 150,
            r: 150,
            t: 150,
            b: 150
        }
    };
    Plotly.newPlot("plot", data, layout);
});
