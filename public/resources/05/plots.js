var us = Object.values(dataLabel.us);
var uk = Object.values(dataLabel.uk);
var canada = Object.values(dataLabel.canada);
var musicLabels = Object.keys(dataLabel.us);
function init() {
    var trace = {
        values: us,
        labels: musicLabels,
        type: 'pie'
    };
    var layout = {
        title: "Music Labels"
    };
    var data = [trace];
    Plotly.newPlot("pie", data, layout);
}
function updatePage(event) {
    var dataset = d3.select("#selectOption").property("value");
    var values = us;
    if (dataset === "uk") {
        values = uk;
    }
    else if (dataset === "us") {
        values = us;
    }
    else if (dataset === "canada") {
        values = canada;
    }
    Plotly.restyle("pie", "values", [values]);
}
d3.select("#selectOption").on("change", updatePage);
init();